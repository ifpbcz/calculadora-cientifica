//Funções de manipulação

function inserir(n){
    document.getElementById("input").value += n;
}

function limpar(){
    document.getElementById("input").value = "";
    document.getElementById("output").value = "";	
}

function backspace(){
    var exp = document.getElementById("input").value;
    document.getElementById("input").value = exp.substring(0,exp.length-1);
} 

function igual(){
    var exp = document.getElementById("input").value;
    if (exp) {
        document.getElementById("output").value = eval(exp);
        exp.value = "";
    }
}


//Funções de cálculo

function seno(){
    var exp = document.getElementById("input").value;
    if (exp){
        exp = eval(exp);
        document.getElementById("input").value =  Math.sin(exp);
    }
}

function cosseno(){
    var exp = document.getElementById("input").value;
    if (exp){
        exp = eval(exp);
        document.getElementById("input").value = Math.cos(exp);
    }
}

function tangente(){
    var exp = document.getElementById("input").value;
    if (exp){
        exp = eval(exp);
        document.getElementById("input").value = Math.tan(exp);
    }
}

function quadrado(){
    var exp = document.getElementById("input").value;
    if (exp){
        exp = eval(exp);
        document.getElementById("input").value = Math.sqrt(exp);
    }
}

function potencia(){
    var exp = document.getElementById("input").value;
    if (exp){
        exp = eval(exp);    
        document.getElementById("input").value = Math.pow(exp, 2);
    }
}

function modulo(){
    var exp = document.getElementById("input").value;
    if (exp){
        exp = eval(exp);
        document.getElementById("input").value = Math.abs(exp);
    }
}

//Logarítimo na base 'e'
function logaritimoNatural(num){
    var exp = document.getElementById("input").value;
    if (exp){
        exp = eval(exp);
        document.getElementById("input").value = Math.log(num);
    }
}

//Logarítimo na base 10
function logaritimo(num){
    var exp = document.getElementById("input").value;
    if (exp){
        exp = eval(exp);
        document.getElementById("input").value = Math.log10(num);
    }
}